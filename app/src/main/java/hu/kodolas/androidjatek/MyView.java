package hu.kodolas.androidjatek;


import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

public class MyView extends View {

    /** Ennyi ms-onként (ezredmásodperc) rajzoljuk ki a képet */
    private static final int INTERVAL = 20;

    /** Piros tinta */
    private static final Paint RED = new Paint();
    /** Zöld tinta */
    private static final Paint GREEN = new Paint();

    static {
        RED.setColor(Color.RED);
        GREEN.setColor(Color.GREEN);
    }

    /** Éppen hozzáér a játékos a képernyőhöz? */
    private boolean touched = false;
    /** Az aktuális érintés koordinátái */
    private float touchX, touchY;

    /**
     * Egy labdát reprezentáló PhysicalObject (TömegPont)
     */
    private PhysicalObject ball;

    /** Konstruktor */
    public MyView(Context context) {
        super(context);
        init();
    }

    /** Konstruktor */
    public MyView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    /**
     * A 'panel' létrejöttekor hívódik meg. Beállítja az érintés figyelését, illetve alapértelmezett értéket ad a
     * játékelemeket.
     */
    private void init() {
        setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        touched = true;
                        touchX = event.getX();
                        touchY = event.getY();
                        break;
                    case MotionEvent.ACTION_UP:
                        touched = false;
                        break;
                }
                return true;
            }
        });
        ball = new PhysicalObject();
        ball.m = 0.5;
    }

    /**
     * Irányítja az egyes játékelemeket, hogy a következő képkockán jó helyre kerüljenek
     */
    private void step() {
        if(touched) { // éppen hozzáér a játékos a képernyőhöz?
            if (touchX < getWidth() / 2.0) // a képernyő baloldalát érinti?
                ball.force(-10.0);
            else // a képernyő jobboldalát érinti
                ball.force(+10.0);
        } else { // nem ért hozzá
            ball.force(0.0);
        }
        ball.step(INTERVAL / 1000.0); // a gyorsulás és a tömeg ismeretében számoljuk ki, hová kerül néhány ms múlva
    }

    /**
     * Néhány ms-onként meghívódik, és arra kér bennünket az Android, hogy rajzoljuk rá a vászonra, hogy éppen hogyan
     * néz ki a játék
     * @param canvas A vászon
     */
    @Override
    protected void onDraw(Canvas canvas) {

        int width = canvas.getWidth(); // a rajzvászon szélessége
        int height = canvas.getHeight(); // a rajzvászon magassága

        step(); // számoltassuk ki, hogy hová kell kerülnie a játékelemeknek!

        canvas.translate(width/2, height/2); // a rajzvászont (mint egy papírdarabot) középre toljuk
        canvas.scale(100f, 100f);            // és 100-szorosára nagyítjuk. Amit ezután rá rajzolunk, 100x-os
                                                     // méretben fog megjelenni

        float x = (float) ball.x; // a labda x pozícióját felhasználva rajzolunk a megf. helyre egy négyzetet
                                  // (ball.x double típusú, ezért átalakítjuk float-tá)

        float size = 1f; // mekkora legyen a négyzet?

        canvas.drawRect(x-size/2, -size/2, x+size/2, +size/2, RED); // drawRect = draw rectangle
                                                        // (téglalap), bal felső sarok, majd a jobb alsó sarok
                                                        // koordinátái

        postInvalidateDelayed(INTERVAL);    // néhány ms múlva ismét kérjük, hogy rajzoltassa velünk újra az Android
                                            // a képernyőt.
    }
}
