package hu.kodolas.androidjatek;

public class PhysicalObject {

    public double x, v, a, m;

    public void force(double f) {
        a = f / m;
    }

    public void step(double dt) {
        v += a * dt;
        x += v * dt;
    }

}
